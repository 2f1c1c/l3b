import express from 'express';
import fetch from 'isomorphic-fetch';
const _ = require('lodash');
const url = require('url');

const app = express();


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.get('/pets/populate', async function(req, res) {  

  let jsObj = await getJson();
  let pathString = await req.path;
  pathString = pathString.slice(1, pathString.lastIndexOf('/'));
  let response = objFind(jsObj, pathString);
  for (let key in response) {
    response[key]['user'] = await userById(jsObj, response[key].userId);
  }
  response = queryFilter(jsObj, response, url.parse(req.url, true).query);
  res.json(response);
 
});

app.get('/pets/:id/populate', async function(req, res) { 
 
  let jsObj = await getJson();
  let pathString = await req.path;
  pathString = pathString.slice(1, pathString.lastIndexOf('/'));
  let response = objFind(jsObj, pathString);
  response['user'] = await userById(jsObj, response.userId);
  response = queryFilter(jsObj, response, url.parse(req.url, true).query); 
  res.json(response);
   
});

app.get('/users/:id/populate', async function(req, res) {  

  let jsObj = await getJson();
  let pathString = await req.path;
  pathString = pathString.slice(1, pathString.lastIndexOf('/'));
  let response = objFind(jsObj, pathString);
  if (!Number.isInteger(+req.params.id)) {
    response = await userByName(jsObj, req.params.id);
  }
  
  if (response === undefined ) {
    res.status(404).send('Not Found');
  } else {
    response['pets'] = await petById(jsObj, response.id);
    response = queryFilter(jsObj, response, url.parse(req.url, true).query);
    res.json(response);
  }
 
});

app.get('/users/populate', async function(req, res) {  

  let jsObj = await getJson();
  let pathString = await req.path;
  pathString = pathString.slice(1, pathString.lastIndexOf('/'));
  let response = objFind(jsObj, pathString);
  for (let key in response) {
    response[key]['pets'] = await petById(jsObj, response[key].id);
  }
  response = queryFilter(jsObj, response, url.parse(req.url, true).query);
  res.json(response);
 
});


app.get('/', async (req,res) =>  {
  res.json(await getJson());
});


app.get('/*', async function(req, res) {

  let jsObj = await getJson();
  let pathString = req.params[0];
  let response = objFind(jsObj, pathString);
  let pathArray = pathString.split('/');
  if (!Number.isInteger(+pathArray[1]) && pathArray.length > 1) {
    response = await userByName(jsObj, pathArray[1]);
  }
  response = queryFilter(jsObj, response, url.parse(req.url, true).query);
  if (response === undefined ) {
    res.status(404).send('Not Found');
  } else {
    console.log("resp: " + response);
    res.json(response);
  }

});


app.listen(3000, function() {
});

async function getJson() {

  const pcUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';
  
  let pc = await fetch(pcUrl).then(async (res) => {
    return res.json();
  }).catch(err => {
    console.log('Something wrong:', err);
  });
    
  return pc;

}

async function userById(obj, id) {
  
  let response = await objFind(obj, 'users'); 
  let res =  _.filter(response, user => user.id === id);
  return res[0];

}

async function userByName(obj, name) {

  let response = await objFind(obj, 'users'); 
  response =  _.filter(response, user => user.username === name);
  return response[0];

}

async function petById(obj, id) {
  
  let response = await objFind(obj, 'pets'); 
  return _.filter(response, pet => pet.userId === id);

}

function queryFilter(objAll, obj, queryObj) {

  if (queryObj['type']) obj = _.filter(obj, { type: queryObj['type'] });
  if (queryObj['age_gt']) obj = _.filter(obj, item => item.age > queryObj['age_gt']);
  if (queryObj['age_lt']) obj = _.filter(obj, item => item.age < queryObj['age_lt']);
  if (queryObj['havePet']) {
    obj = _.filter(obj, item => { 
      for (let key in objAll.pets) {
        if (objAll.pets[key].type === queryObj['havePet'] && objAll.pets[key].userId === item.id) {return true;}}
    });
  }
  return obj;
}

function objFind (obj, pathString) {

  if (pathString.length === pathString.lastIndexOf('/')+1) {
    pathString = pathString.slice(0, pathString.length -1);
   }
  let pathArray = pathString.split('/');
  var current = obj;
  for (let i=0; i < pathArray.length; i++) { 
    if (Number.isInteger(+pathArray[i])) {
      pathArray[i] = +pathArray[i] - 1;
    }
  }
  
  try {
  for (let i = 0; i < pathArray.length; ++i) {
 
    let type = typeof current;
    if (current[pathArray[i]] === undefined) {
      return undefined;
    } else if (type === 'object') {
      current = current[pathArray[i]];
    } else {
      return undefined;
    }
  }
    return current;
  } catch(err) {
    return undefined;
  }
}


